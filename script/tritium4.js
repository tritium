var out = "tritium4-content";
function json_request(url,method,params,output,type)
{
	new Ajax.Request("./?show="+url,
	{
		method: method,
		parameters: params,
		evalScripts: true,
		onSuccess: function(data)
		{
			var json = data.responseText.evalJSON();
			if (type == 1)
			{
				var content = '<ul class="comment-list">';
				for (i = 0; i < json.length; ++i)
				{
					if (!json[i]["retweeted_status"])
					{
						var chunk = "user";
						var author = json[i][chunk].screen_name;
						var avatar = json[i][chunk].profile_image_url;
						var context = parse_tweet(json[i].text);
						var date = json[i].created_at;
						var id = json[i].id;
						var name = json[i][chunk].name;
						var retweeted = false;
						var source = json[i].source;
						var userid = json[i][chunk].id;
					}
					else
					{
						var chunk = "retweeted_status";
						var author = json[i][chunk]["user"].screen_name;
						var avatar = json[i][chunk]["user"].profile_image_url;
						var context = parse_tweet(json[i][chunk].text);
						var date = json[i][chunk].created_at;
						var id = json[i][chunk]["user"].id;
						var name = json[i][chunk]["user"].name;
						var retweeted = "<strong>RT</strong> by "+name;
						var source = json[i][chunk].source;
						var userid = json[i][chunk]["user"].id;
					}
					date = relative_time(date);
					content += '<li class="comment comment-parent" id="comment-'+id+'">';
						content += '<div class="comment-profile-wrapper left">';
							content += '<div class="comment-profile">';
								content += '<div class="comment-gravatar">';
									content += '<img src="'+avatar+'" height="30px" width="30px" alt="" />';
								content += '</div>';
								content += '<div class="comment-author">'+author+'</div>';
							content += '</div>';
						content += '</div>';
						content += '<div class="comment-content-wrapper right">';
							content += '<div class="comment-content-wrapper-2">';
								content += '<div class="comment-body">';
									content += '<div class="comment-arrow"></div>';
									content += '<div id="comment-'+id+'-date" class="post-date">';
										content += '<div class="left">'+date+'</div>';
										/*content += '<div class="right"></div>';*/
										content += '<div class="clearer">&nbsp;</div>';
									content += '</div>';
									content += '<div class="hr"></div>';
									content += '<div class="comment-text">';
										content += '<p id="comment-'+id+'-text">'+context+'</p>';
									content += '</div>';
									content += '<div class="clearer">&nbsp;</div>';
								content += '</div>';
							content += '</div>';
						content += '</div>';
						content += '<div class="clearer">&nbsp;</div>';
					content += '</li>';
				}
				content += '</ul>';
				$(output).update(content);
			}
			else
			{
				return json;
			}
		}
	});
}
function loadhome(params)
{
	new Effect.Appear("loading",
	{
		afterFinish: function()
		{
			var content = json_request("home","GET",params,out,1);
			new Effect.Fade("loading",
			{
				afterFinish: function()
				{
					$("loading").setStyle("display:none;");
					return content;
				},
				duration: .4
			});
		},
		duration: .2
	});
}
function loadmore(what,method,params,type)
{
	
}
//from the official twitter widget, (C) 2010 twitter
var relative_time = function (a)
{
	var K = function ()
	{
		var a = navigator.userAgent;
		return{
			ie: a.match(/MSIE\s([^;]*)/)
		}
	}();
	var b = new Date();
	var c = new Date(a);
	if (K.ie)
	{
		c = Date.parse(a.replace(/( \+)/, ' UTC$1'))
	}
	var d = b - c;
	var e = 1000, minute = e * 60, hour = minute * 60, day = hour * 24, week = day * 7, month = day * 30, year = month * 12;
	if (isNaN(d) || d < 0)
	{
		return "";
	}
	if (d < e * 7)
	{
		return "just now";
	}
	if (d < minute)
	{
		return Math.floor(d / e) + " secs ago";
	}
	if (d < minute * 2)
	{
		return "a min ago";
	}
	if (d < hour)
	{
		return Math.floor(d / minute) + " mins ago";
	}
	if (d < hour * 2)
	{
		return "an hr ago";
	}
	if (d < day)
	{
		return Math.floor(d / hour) + " hrs ago";
	}
	if (d > day && d < day * 2)
	{
		return "a day ago";
	}
	if (d < week)
	{
		return Math.floor(d / day) + " days ago";
	}
	if (d > week && d < week * 2)
	{
		return "last week";
	}
	if (d > week && d < month)
	{
		return Math.floor(d / week) + " weeks ago";
	}
	if (d > month && d < month * 2)
	{
		return "last month";
	}
	if (d > month)
	{
		return Math.floor(d / month) + " months ago";
	}
	if (d > year && d < year * 2)
	{
		return "last year";
	}
	if (d > year)
	{
		return Math.floor(d / year) + " years ago";
	}
};

function parse_tweet(text)
{
	if(!text)
	{
		return text;
	}
	else
	{
		text = text.replace(/((https?\:\/\/)|(www\.))([^ ]+)/g,function(url)
		{
			return '<a target="_blank" rel="nofollow" href="'+ url +'">'+url.replace(/^www./i,'')+'</a>';
		});
		text = text.replace(/@([\w*]+)/g,function(user)
		{
			return '<a target="_blank" rel="nofollow" href="http://twitter.com/'+user+'">'+user+'</a>';
		})+" ";
		text = text.replace(/#([\w*]+)/g,function(tag)
		{
			return '<a target="_blank" rel="nofollow" href="http://search.twitter.com/search?q='+tag.replace(/#/i,'%23')+'">'+tag+'</a>';
		})+" ";
		return text;
	}
}

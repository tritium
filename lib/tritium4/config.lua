--[[
consumer information (register your app at twitter.com/apps
]]--
consumer_key = ""
consumer_secret = ""

--[[
google stuff
]]--
google_api_key = "ABQIAAAAIwuVMEXuk-m6djJbJCi-rRT14FIEZj_Ds5A1V-U72dB9nZ-JhhRbwLjsZsmS26Y4LwwSwnLojbdgvg"


--[[
tritium4 stuff
]]--
redirection = ""

--[[
tthe below contains some base information about where we will make requests to at twitter
]]--
access_token = "https://api.twitter.com/oauth/access_token"
authorize_user = "https://api.twitter.com/oauth/authorize"
request_token = "https://api.twitter.com/oauth/request_token"

--[[
the below contains 
]]--
oauth_token = tostring(cgilua.cookies.get("oauth_token"))
oauth_token_secret = tostring(cgilua.cookies.get("oauth_token_secret"))
temp_oauth_token = tostring(cgilua.cookies.get("temp_oauth_token"))
temp_oauth_token_secret = tostring(cgilua.cookies.get("temp_oauth_token_secret"))
screen_name = tostring(cgilua.cookies.get("screen_name"))
user_id = tostring(cgilua.cookies.get("user_id"))

--[[
twitter resources (from ltwitter)
]]--
resources = {
	-- Timeline resources
	publicTimeline = {"GET", "statuses/public_timeline"},
	homeTimeline = {"GET", "statuses/home_timeline"},
	friendsTimeline = {"GET", "statuses/friends_timeline"},
	userTimeline = {"GET", "statuses/user_timeline"},
	mentions = {"GET", "statuses/mentions"},
	retweetedByMe = {"GET", "statuses/retweeted_by_me"},
	retweetedToMe = {"GET", "statuses/retweeted_to_me"},
	-- Tweets resources
	retweetsOfMe = {"GET", "statuses/retweets_of_me"},
	showStatus = {"GET", "statuses/show"},
	updateStatus = {"POST", "statuses/update"},
	destroyStatus = {"POST", "statuses/destroy"},
	retweetStatus = {"POST", "statuses/retweet/:id"},
	retweets = {"GET", "statuses/retweets"},
	retweetedBy = {"GET", "statuses/:id/retweeted_by"},
	retweetedByIds = {"GET", "statuses/:id/retweeted_by/ids"},
	-- User resources
	showUser = {"GET", "users/show"},
	lookupUsers = {"GET", "users/lookup"},
	searchUsers = {"GET", "users/search"},
	suggestedUserGroups = {"GET", "users/suggestions"},
	suggestedUsers = {"GET", "users/suggestions/:slug"},
	profileImage = {"GET", "users/profile_image/:screen_name"},
	friends = {"GET", "statuses/friends"},
	followers  = {"GET", "statuses/followers"},
	-- Trends resources
	trends = {"GET", "trends"},
	currentTrends = {"GET", "trends/current"},
	dailyTrends = {"GET", "trends/daily"},
	weeklyTrends = {"GET", "trends/weekly"},
	-- List resources
	newList = {"POST", ":user/lists"},
	updateList = {"POST", ":user/lists/:id"},
	userLists = {"GET", ":user/lists"},
	showList = {"GET", ":user/lists/:id"},
	deleteList = {"DELETE", ":user/lists/:id"},
	listTimeline = {"GET", ":user/lists/:id/statuses"},
	listMemberships = {"GET", ":user/lists/memberships"},
	listSubscriptions = {"GET", ":user/lists/subscriptions"},
	-- List Members resources
	getListMembers = {"GET", ":user/:list_id/members"},
	addListMember = {"POST", ":user/:list_id/members"},
	delListMember = {"DELETE", ":user/:list_id/members"},
	chkListMember = {"GET", ":user/:list_id/members/:id"},
	-- List Subscribers resources
	getListSubscribers = {"GET", ":user/:list_id/subscribers"},
	addListSubscriber = {"POST", ":user/:list_id/subscribers"},
	delListSubscriber = {"DELETE", ":user/:list_id/subscribers"},
	chkListSubscriber = {"GET", ":user/:list_id/subscribers/:id"},
	-- Direct Messages resources
	listMessages = {"GET", "direct_messages"},
	sentMessages = {"GET", "direct_messages/sent"},
	sendMessage = {"POST", "direct_messages/new"},
	removeMessage = {"POST", "direct_messages/destroy"},
	-- Friendship resources
	follow = {"POST", "friendships/create/:id"},
	unfollow = {"POST", "friendships/destroy/:id"},
	isFollowing = {"GET", "friendships/exists"},
	showRelation = {"GET", "friendships/show"},
	inFriendships = {"GET", "friendships/incoming"},
	outFriendships = {"GET", "friendships/outgoing"},
	-- Friends and Followers resources
	following = {"GET", "friends/ids"},
	followers = {"GET", "followers/ids"},
	-- Account resources
	rateLimitStatus = {"GET", "account/rate_limit_status"},
	updateDeliveryDevice = {"POST", "account/update_delivery_device"},
	updateProfileColors = {"POST", "account/update_profile_colors"},
	updateProfileImage = {"POST", "account/update_profile_image"},
	updateProfileBackground = {"POST", "account/update_profile_background"},
	updateProfile = {"POST", "account/update_profile"},
	-- Favorites resources
	favorites = {"GET", "favorites"},
	addFavorite = {"POST", "favorites/:id/create"},
	delFavorite = {"POST", "favorites/destroy"},
	-- Notifications resources
	enableNotifications = {"POST", "notifications/follow"},
	disableNotifications = {"POST", "notifications/unfollow"},
	-- Block resources
	addBlock = {"POST", "blocks/create"},
	delBlock = {"POST", "blocks/destroy"},
	chkBlock = {"GET", "blocks/exists"},
	blocking = {"GET", "blocks/blocking"},
	blockingIds = {"GET", "blocks/blocking/ids"},
	-- Spam Reporting resources
	reportSpam = {"POST", "report_spam"},
	-- Saved Searches resources
	savedSearches = {"GET", "saved_searches"},
	doSavedSearch = {"GET", "saved_searches/show"},
	addSavedSearch = {"POST", "saved_searches/create"},
	delSavedSearch = {"POST", "saved_searches/destroy"},
	-- Local Trends resources
	availableLocations = {"GET", "trends/available"},
	locationTrends = {"GET", "trends/locations/:woeid"},
	-- Geo resources
	reverseGeocode = {"GET", "geo/reverse_geocode"},
	placeInfo = {"GET", "geo/id/:place_id"}
}

--[[
after merging the above table, we callback to this to make the request
this make its easier to do future toggling between sites such as twitter, identica, etc.
]]--
twitter = {
	__index = function(tbl, method)
		if not resources[method] then return nil end
			local resource = resources[method]
			local url = "https://api.twitter.com/1/"..resource[2]..".json"
			return function(self, args)
			args = args or {}
			local url = url:gsub(":([%w_-]+)", function (s)
				if args[s] then
					local ret = args[s]
					args[s] = nil
					return ret
				else
					return s
				end
			end)
			for k,v in pairs(args) do
				args[k] = tostring(v)
			end
			local _, _, _, body = self.oauth_client:PerformRequest(resource[1], url, args)
			return body
		end
	end
}

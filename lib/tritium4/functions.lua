json = require("json")
oauth = require("OAuth")

function connect()
	local request = request1()
	values = request:RequestToken()
	local new_oauth_token = tostring(values.oauth_token)
	local new_oauth_token_secret = tostring(values.oauth_token_secret)
	local new_url = request:BuildAuthorizationUrl()
	cgilua.cookies.set("temp_oauth_token",new_oauth_token)
	cgilua.cookies.set("temp_oauth_token_secret",new_oauth_token_secret)
	cgilua.redirect(new_url)
end

function callback(move)
	local request = request1()
	request:SetToken(temp_oauth_token)
	request:SetTokenSecret(temp_oauth_token_secret)
	values = request:GetAccessToken()
	local new_oauth_token = tostring(values.oauth_token)
	local new_oauth_token_secret = tostring(values.oauth_token_secret)
	local user_id = tostring(values.user_id)
	local screen_name = tostring(values.screen_name)
	cgilua.cookies.set("oauth_token",new_oauth_token)
	cgilua.cookies.set("oauth_token_secret",new_oauth_token_secret)
	cgilua.cookies.set("screen_name",screen_name)
	cgilua.cookies.set("user_id",user_id)
	cgilua.cookies.delete("temp_oauth_token")
	cgilua.cookies.delete("temp_oauth_token_secret")
	cgilua.redirect(move)
end

function footer()
	cgilua.put([[</body></html>]])
end

function header(title,header)
	cgilua.put([[<!doctype html>
<html lang="en">
	<head>
		<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Cantarell&subset=latin" />
		<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lobster&subset=latin" />
		<link rel="stylesheet" type="text/css" href='https://fonts.googleapis.com/css?family=Molengo&subset=latin' />
		<link rel="stylesheet" href="/css/tritium4.css" media="all" />
		<meta charset="utf-8" />
		<meta name="viewport" content="initial-scale=1.0, width=device-width, maximum-scale=1.0" />
		<script src="https://www.google.com/jsapi?key=]])
	cgilua.put(google_api_key)
	cgilua.put([[" type="text/javascript"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/prototype/1.6.1.0/prototype.js" type="text/javascript"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/scriptaculous/1.8.3/scriptaculous.js" type="text/javascript"></script>
		<script src="/script/tritium4.js" type="text/javascript"></script>
		<title>]])
	if header ~= "nil" then
		cgilua.put("@"..header.." on "..title)
	else
		cgilua.put(title)
	end
	cgilua.put([[</title>
	</head>
	<body>
		<h1>]])
	if header ~= "nil" then
		cgilua.put("@"..header)
	else
		cgilua.put(header)
	end
	cgilua.put([[</h1>
	]])
end

function loggedin()
	cgilua.put('<div>')
	if (oauth_token ~= "nil" and oauth_token_secret ~= "nil") then
		cgilua.put('<div id="loading" style="text-align:center"><img src="/css/loading.gif" alt="" /></div>');
		cgilua.put('<div id="tritium4-content"></div>')
		cgilua.put('<script>loadhome(null);</script>')
	else
		cgilua.put('<li class="comment comment-parent" style="text-align:center"><a href="./?exec=connect"><img src="/css/signin.png" alt="" /></a></li>')
	end
	cgilua.put('</div>')
end

function logout(move)
	cgilua.cookies.delete("oauth_token")
	cgilua.cookies.delete("oauth_token_secret")
	cgilua.cookies.delete("screen_name")
	cgilua.cookies.delete("temp_oauth_token")
	cgilua.cookies.delete("temp_oauth_token_secret")
	cgilua.cookies.delete("user_id")
	cgilua.redirect(move)
end

function request1()
	local oauth_client = oauth.new(consumer_key,consumer_secret,
	{
		AccessToken = access_token,
		AuthorizeUser = authorize_user,
		RequestToken = request_token
	})
	return oauth_client
end

function request2()
	local o = setmetatable({}, twitter)
	o.oauth_client = oauth.new(consumer_key,consumer_secret,
	{
		AccessToken = access_token,
		AuthorizeUser = authorize_user,
		RequestToken = request_token
	},
	{
		OAuthToken = oauth_token,
		OAuthTokenSecret = oauth_token_secret
	})
	return o
end

function switch(case)
	return function(codetable)
		local f = codetable[case] or codetable.default
		if f then
			if type(f) == "function" then
				return f(case)
			else
				error("case "..tostring(case).." not a function")
			end
		end
	end
end

function timeline(method,params)
	local request = request2()
	switch(method)
	{
		["home"] = function() rx = request:homeTimeline{params} end,
		["profile"] = function() rx = request:userTimeline{params} end,
		["replies"] = function() rx = request:mentions{params} end,
		default = function() local rx = request:homeTimeline{params} end
	}
	cgilua.contentheader("application","json")
	cgilua.put(tostring(rx))
end
